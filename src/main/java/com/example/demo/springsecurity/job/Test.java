package com.example.demo.springsecurity.job;

import com.example.demo.springsecurity.config.quartz.AbstractQuartzJobBean;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: liucheng
 * @createTime:2020/6/30 19:42
 */
@Component
public class Test extends AbstractQuartzJobBean {
    @Override
    protected String getJobName() {
        return "test";
    }

    @Override
    protected String getJobGroup() {
        return "test";
    }

    @Override
    protected String getCron() {
        return "*/5 * * * * ?";
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("test");
    }
}

