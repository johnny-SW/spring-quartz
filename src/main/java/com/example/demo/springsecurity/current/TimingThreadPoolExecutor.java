package com.example.demo.springsecurity.current;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @description:一个提供计时的线程池，它继承 {@link ThreadPoolExecutor}。在 {@link ThreadPoolExecutor}的基础上添加了每次任务
 * 执行耗时，所有任务平均耗时的统计，还有线程池拒绝任务数的统计（前提是你使用 {@link CountCallerRunsPolicy}作为任务拒绝策略，当然
 * 这个线程池使用它作为默认拒绝策略）。这个类不提供分别统计不同任务的耗时功能，
 * 可以使用{@link com.example.demo.springsecurity.current.test.NameTimeThreadPoolExecutor}来实现这个功能。
 * <p>
 * 如果想方便使用这个线程池，可以使用已经提供的工厂类 {@link Executors}。里面有一些默认的实现。
 * <p>
 * 这个类使用 {@code numTasks},{@code totalTime},{@code rejectNum}来分别记录执行任务总数，执行总耗时，拒绝任务数。
 * <p>
 * 可以通过{@code dealExecuteInfo}方法获取到当前线程池执行任务的情况。这个方法需要一个参数 {@link ExecuteInfoHandler},
 * {@link ExecuteInfoHandler}通过实现{@link ExecuteInfoHandler}里的{@code dealInfo}方法去定制怎么处理消息（打印或放入数据库等）。
 *
 *
 * @author: liucheng
 * @createTime:2020/5/20 14:39
 */
public class TimingThreadPoolExecutor extends AbstractTimingThreadPoolExecutor {
    private static final Logger logger = LoggerFactory.getLogger(TimingThreadPoolExecutor.class);
    /**
     * 任务总数
     */
    private final AtomicLong numTasks = new AtomicLong();
    /**
     * 总耗时
     */
    private final AtomicLong totalTime = new AtomicLong();
    /**
     * 拒绝任务数
     */
    private final AtomicLong rejectNum = new AtomicLong();

    public TimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                    TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    public TimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                    TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, new CountCallerRunsPolicy());
    }

    public TimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    public TimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler, ExecuteInfoWriter logWriter) {
        super(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler, logWriter);
    }


    @Override
    public long incrementAndGetRejectNum(Runnable task) {
        return rejectNum.incrementAndGet();
    }

    @Override
    protected long incrementAndGetNumTasks(Runnable task) {
        return numTasks.incrementAndGet();
    }

    @Override
    protected long addAndGetTotalTime(long spendTime, Runnable task) {
        return totalTime.addAndGet(spendTime);
    }

    @Override
    public void dealExecuteInfo(ExecuteInfoHandler handler) {
        handler.dealInfo(this.getPoolName(), null, numTasks.get(), totalTime.get(), rejectNum.get());
    }

    @Override
    public Object getLockObject(Runnable runnable) {
        return numTasks;
    }
}


