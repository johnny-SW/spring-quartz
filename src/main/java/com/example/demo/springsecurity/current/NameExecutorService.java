package com.example.demo.springsecurity.current;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/**
 * @description:带有名称的任务 执行服务
 * @author: liucheng
 * @createTime:2020/7/3 9:55
 */
public interface NameExecutorService extends ExecutorService {
    /**
     * 执行带有名称的任务
     *
     * @param name
     * @param runnable
     * @return
     */
    NameFutureTask<?> submit(String name, Runnable runnable);

    /**
     * 执行带有名称的任务
     *
     * @param name
     * @param callable
     * @return
     */
    <T> NameFutureTask<T> submit(String name, Callable<T> callable);
}
