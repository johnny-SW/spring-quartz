package com.example.demo.springsecurity.current.test;

import com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor;
import com.example.demo.springsecurity.current.Executors;
import com.example.demo.springsecurity.current.NameExecutorService;
import com.example.demo.springsecurity.current.NameTimingThreadPoolExecutor;

import java.util.concurrent.ExecutorService;

/**
 * @description:
 * @author: liucheng
 * @createTime:2020/7/3 17:37
 */
public class TestExecutors {
    public static void main(String[] args) {
        NameTimingThreadPoolExecutor executorService = (NameTimingThreadPoolExecutor) Executors.newFixedNameTimingThreadPool("测试测试", 2);
        executorService.submit("haha", () -> System.out.println("测试newFixedNameTimingThreadPool11"));
        executorService.submit("lala", () -> System.out.println("测试newFixedNameTimingThreadPool22"));
        executorService.submit("lala", () -> System.out.println("测试newFixedNameTimingThreadPool22"));
        ((AbstractTimingThreadPoolExecutor) executorService).dealExecuteInfo(executorService.getExecuteWriter());
        executorService.shutdown();


    }
}
