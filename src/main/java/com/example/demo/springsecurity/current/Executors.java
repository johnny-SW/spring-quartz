package com.example.demo.springsecurity.current;

import java.util.concurrent.*;

/**
 * @description:最好自己根据业务进行线程池创建。不要使用这个工厂类提供的方法。
 * @author: liucheng
 * @createTime:2020/7/3 16:58
 */
public class Executors {

    /**
     * 创建一个核心线程数和最大线程数一致的计时线程池。
     * 任务存放队列使用LinkedBlockingQueue（可能会产生无限队列）
     * 拒绝策略默认使用 {@link com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy}
     *
     * @param name     线程池名称
     * @param nThreads 线程数
     * @return
     */
    public static ExecutorService newFixedTimingThreadPool(String name, int nThreads) {
        return new TimingThreadPoolExecutor(name, nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }


    /**
     * 创建一个线程数量固定的计时线程池
     * 任务存放队列使用LinkedBlockingQueue（可能会产生无限队列）
     * 拒绝策略默认使用 {@link com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy}
     *
     * @param nThreads      线程数
     * @param threadFactory 线程工厂
     * @return
     */
    public static ExecutorService newFixedTimingThreadPool(String name, int nThreads, ThreadFactory threadFactory) {
        return new TimingThreadPoolExecutor(name, nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),
                threadFactory);
    }


    /**
     * 创建一个缓存线程的计时线程池。任务提交到这个线程池会被直接执行。要防止任务过多资源耗尽。
     * 一个线程被创建后会缓存60秒。
     * 最大线程数为Integer的最大值。
     * 队列使用SynchronousQueue不会
     *
     * @param name 线程池名称
     * @return
     */
    public static ExecutorService newCachedTimingThreadPool(String name) {
        return new TimingThreadPoolExecutor(name, 0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>());
    }


    /**
     * 创建一个缓存线程的计时线程池。任务提交到这个线程池会被直接执行。要防止任务过多资源耗尽。
     * 一个线程被创建后会缓存60秒。
     * 最大线程数为Integer的最大值。
     * 队列使用SynchronousQueue不会
     *
     * @param name          线程池名称
     * @param threadFactory
     * @return
     */
    public static ExecutorService newCachedTimingThreadPool(String name, ThreadFactory threadFactory) {
        return new TimingThreadPoolExecutor(name, 0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>(),
                threadFactory);
    }


    /**
     * 创建一个核心线程数和最大线程数一致的根据任务名称分别计算耗时时间的线程池。
     * 任务存放队列使用LinkedBlockingQueue（可能会产生无限队列）
     * 拒绝策略默认使用 {@link com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy}
     *
     * @param name     线程池名称
     * @param nThreads 线程数
     * @return
     */
    public static NameExecutorService newFixedNameTimingThreadPool(String name, int nThreads) {
        return new NameTimingThreadPoolExecutor(name, nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }


    /**
     * 创建一个线程数量固定的根据任务名称分别计算耗时时间的线程池。
     * 任务存放队列使用LinkedBlockingQueue（可能会产生无限队列）
     * 拒绝策略默认使用 {@link com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy}
     *
     * @param nThreads      线程数
     * @param threadFactory 线程工厂
     * @return
     */
    public static NameExecutorService newFixedNameTimingThreadPool(String name, int nThreads, ThreadFactory threadFactory) {
        return new NameTimingThreadPoolExecutor(name, nThreads, nThreads,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(),
                threadFactory);
    }


    /**
     * 创建一个缓存线程的根据任务名称分别计算耗时时间的线程池。
     * 任务提交到这个线程池会被直接执行。要防止任务过多资源耗尽。
     * 一个线程被创建后会缓存60秒。
     * 最大线程数为Integer的最大值。
     * 队列使用SynchronousQueue不会
     *
     * @return
     */
    public static NameExecutorService newCachedNameTimingThreadPool(String name) {
        return new NameTimingThreadPoolExecutor(name, 0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>());
    }


    /**
     * 创建一个缓存线程的根据任务名称分别计算耗时时间的线程池。
     * 任务提交到这个线程池会被直接执行。要防止任务过多资源耗尽。
     * 一个线程被创建后会缓存60秒。
     * 最大线程数为Integer的最大值。
     * 队列使用SynchronousQueue不会
     *
     * @param threadFactory
     * @return
     */
    public static NameExecutorService newCachedNameTimingThreadPool(String name, ThreadFactory threadFactory) {
        return new NameTimingThreadPoolExecutor(name, 0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>(),
                threadFactory);
    }
}
