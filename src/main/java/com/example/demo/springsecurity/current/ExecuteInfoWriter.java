package com.example.demo.springsecurity.current;

/**
 * @description:线程池执行过程中信息的记录
 * @author: liucheng
 * @createTime:2020/7/2 17:47
 */
public interface ExecuteInfoWriter extends ExecuteInfoHandler {
    /**
     * 记录线程池每次执行任务后的信息
     *
     * @param poolName         线程池名称
     * @param r         任务
     * @param numTasks         执行任务总数
     * @param totalSpendTime   执行花费总时间 单位：纳秒
     * @param currentSpendTime 当前任务执行耗时 单位：纳秒
     */
    void writeExecuteInfo(String poolName, Runnable r, Long numTasks, Long totalSpendTime, Long currentSpendTime);

    /**
     * 记录线程池执行拒绝任务数
     *
     * @param poolName
     * @param rejectNum
     */
    void writeReject(String poolName, Long rejectNum);
}
