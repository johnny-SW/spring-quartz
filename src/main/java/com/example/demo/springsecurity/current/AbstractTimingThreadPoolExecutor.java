package com.example.demo.springsecurity.current;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * @description:
 * @author: liucheng
 * @createTime:2020/7/2 20:33
 */
public abstract class AbstractTimingThreadPoolExecutor extends ThreadPoolExecutor {

    private static final Logger logger = LoggerFactory.getLogger(com.example.demo.springsecurity.current.TimingThreadPoolExecutor.class);

    private final ThreadLocal<Long> startTime = new ThreadLocal<>();
    /**
     * 线程池名称
     */
    private String name;
    /**
     * 执行信息输出
     */
    protected ExecuteInfoWriter executeInfoWriter;

    public AbstractTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                            TimeUnit unit, BlockingQueue<Runnable> workQueue
    ) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
                new ThreadFactoryBuilder().setNameFormat(name + "-").build(), new CountCallerRunsPolicy());
        this.name = name;
        executeInfoWriter = new LogExecuteInfoWriter();
    }

    public AbstractTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                            TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory
    ) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, new CountCallerRunsPolicy());
        this.name = name;
        executeInfoWriter = new LogExecuteInfoWriter();
    }


    public AbstractTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                            TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,
                                            RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        this.name = name;
        executeInfoWriter = new LogExecuteInfoWriter();
    }

    public AbstractTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                            TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,
                                            RejectedExecutionHandler handler, ExecuteInfoWriter logWriter) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        this.name = name;
        this.executeInfoWriter = logWriter;
    }


    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        startTime.set(System.nanoTime());
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        try {
            long endTime = System.nanoTime();
            long taskTime = endTime - startTime.get();
            long numTasks;
            long totalTime;
            synchronized (getLockObject(r)) {
                numTasks = incrementAndGetNumTasks(r);
                totalTime = addAndGetTotalTime(taskTime, r);
            }
            executeInfoWriter.writeExecuteInfo(name, r, numTasks, totalTime, taskTime);
        } catch (Exception e) {
            logger.error("{} 线程池执行任务后计数异常 error message:{}", name, Throwables.getStackTraceAsString(e));
        } finally {
            super.afterExecute(r, t);
        }
    }


    @Override
    protected void terminated() {
        try {
            dealExecuteInfo(executeInfoWriter);
        } catch (Exception e) {
            logger.error("{} 线程池销毁时计数异常 error message:{}", name, Throwables.getStackTraceAsString(e));
        } finally {
            super.terminated();
        }
    }

    /**
     * 获取线程池名称
     *
     * @return
     */
    public String getPoolName() {
        return this.name;
    }

    /**
     * 增加拒绝数
     *
     * @param task
     * @return
     */
    public abstract long incrementAndGetRejectNum(Runnable task);

    /**
     * 增加并返回任务总数
     *
     * @param task
     * @return
     */
    protected abstract long incrementAndGetNumTasks(Runnable task);

    /**
     * 增加并返回消耗时间
     *
     * @param task
     * @param spendTime
     * @return
     */
    protected abstract long addAndGetTotalTime(long spendTime, Runnable task);

    /**
     * 处理执行信息
     *
     * @param handler
     */
    public abstract void dealExecuteInfo(ExecuteInfoHandler handler);

    /**
     * 获取加锁对象（用于保证写出执行数据一致性）
     *
     * @param runnable
     * @return
     */
    public abstract Object getLockObject(Runnable runnable);

    public ExecuteInfoWriter getExecuteWriter() {
        return this.executeInfoWriter;
    }

    public static class CountCallerRunsPolicy extends CallerRunsPolicy {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            try {
                if (e instanceof com.example.demo.springsecurity.current.TimingThreadPoolExecutor) {
                    com.example.demo.springsecurity.current.TimingThreadPoolExecutor executor = (com.example.demo.springsecurity.current.TimingThreadPoolExecutor) e;

                    executor.getExecuteWriter().writeReject(executor.getPoolName(), executor.incrementAndGetRejectNum(r));


                }
            } catch (Exception ex) {
                logger.error("拒绝策略异常 error message:{}", Throwables.getStackTraceAsString(ex));
            } finally {
                super.rejectedExecution(r, e);
            }


        }
    }

    static class LogExecuteInfoWriter implements ExecuteInfoWriter {
        @Override
        public void writeExecuteInfo(String poolName, Runnable runnable, Long numTasks, Long totalSpendTime, Long currentSpendTime) {
            logger.info("线程池：{},总任务数：{}，平均执行耗时：{}μs，本次执行耗时：{}μs", poolName, numTasks, (totalSpendTime / numTasks) / 1000, currentSpendTime / 1000);
        }

        @Override
        public void writeReject(String poolName, Long rejectNum) {
            logger.warn("{} reject task num :{}", poolName, rejectNum);
        }

        @Override
        public void dealInfo(String poolName, String taskName, Long numTasks, Long totalSpendTime, Long rejectNum) {
            if (numTasks == 0) {
                return;
            }
            logger.info("线程池：{}，总耗时：{}μs，平均耗时:{}μs,线程池总拒绝数：{}", poolName, totalSpendTime / 1000, (totalSpendTime / numTasks) / 1000, rejectNum);

        }
    }
}



