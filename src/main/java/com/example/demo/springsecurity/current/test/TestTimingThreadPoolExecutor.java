package com.example.demo.springsecurity.current.test;

import com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor;
import com.example.demo.springsecurity.current.TimingThreadPoolExecutor;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.ArrayList;
import java.util.concurrent.*;

/**
 * @description:
 * @author: liucheng
 * @createTime:2020/7/3 10:54
 */
public class TestTimingThreadPoolExecutor {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = new TimingThreadPoolExecutor("TestTimingThreadPoolExecutor", 30, 30, 0L,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100),
                new ThreadFactoryBuilder().setNameFormat("testTiming").build(),
                new AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy());

        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                executor.execute(() -> {
                    System.out.println("哈哈");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }).start();
        }

        TimeUnit.SECONDS.sleep(50);
        executor.shutdown();
    }
}
