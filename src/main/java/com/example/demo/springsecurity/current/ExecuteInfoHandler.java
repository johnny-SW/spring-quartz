package com.example.demo.springsecurity.current;

/**
 * @description:在调用 {@link AbstractTimingThreadPoolExecutor}的dealExecuteInfo方法时使用。
 * 用来对线程池执行任务信息进行处理
 * @author: liucheng
 * @createTime:2020/7/2 21:06
 */
public interface ExecuteInfoHandler {
    /**
     * 将线程池的处理任务的信息进行处理（打印或放入数据库等）
     *
     * @param poolName  线程池名称
     * @param taskName  任务名称
     * @param numTasks  任务数
     * @param totalSpendTime 总执行时间
     * @param rejectNum 拒绝任务数
     */
    void dealInfo(String poolName, String taskName, Long numTasks, Long totalSpendTime, Long rejectNum);
}
