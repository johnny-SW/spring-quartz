package com.example.demo.springsecurity.current;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @description:带有名称的Future
 * @author: liucheng
 * @createTime:2020/7/2 17:09
 */
public class NameFutureTask<T> extends FutureTask<T> {
    private String name;

    public NameFutureTask(String name, Callable<T> callable) {
        super(callable);
        this.name = name;
    }

    public NameFutureTask(String name, Runnable runnable, T result) {
        super(runnable, result);
        this.name = name;
    }

    public NameFutureTask(String name, Runnable runnable) {
        this(name, runnable, null);
    }

    public String getName() {
        return name;
    }

}
