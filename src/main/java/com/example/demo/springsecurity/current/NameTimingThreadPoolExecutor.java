package com.example.demo.springsecurity.current;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @description:一个提供计时的线程池，它继承 {@link ThreadPoolExecutor}。在 {@link ThreadPoolExecutor}的基础上添加了每次任务
 * 执行耗时，<b相同名称任务</b>平均耗时的统计，还有线程池拒绝任务数的统计（前提是你使用 {@link CountCallerRunsPolicy}作为任务拒绝策略，当然
 * 这个线程池使用它作为默认拒绝策略）。
 * <p>
 * 如果想方便使用这个线程池，可以使用已经提供的工厂类 {@link Executors}。里面有一些默认的实现。
 * <p>
 * 这个类使用 {@code numTasksMap},{@code totalTimeMap},{@code rejectNum}来分别记录执行任务总数，执行总耗时，拒绝任务数。
 * numTasksMap，totalTimeMap是{@link ConcurrentHashMap},通过相应任务的名称获取到各自的任务数和执行耗时。
 * <p>
 * 实现了{@link NameExecutorService}，通过{@link NameExecutorService}的submit(String name, Runnable runnable)方法来提交任务。
 * 不同name的任务会分别进行统计和计算平均值。
 * 当然可以通过{@link ExecutorService}的submit来提交任务，但是它会把通过{@link ExecutorService}提交的任务都使用 DEFAULT_TASK_NAME
 * 来作为名称进行统一处理。
 * <p>
 * 可以通过{@code dealExecuteInfo}方法获取到当前线程池执行任务的情况。这个方法需要一个参数 {@link ExecuteInfoHandler},
 * {@link ExecuteInfoHandler}通过实现{@link ExecuteInfoHandler}里的{@code dealInfo}方法去定制怎么处理消息（打印或放入数据库等）。
 * @author: liucheng
 * @createTime:2020/7/2 17:42
 */
public class NameTimingThreadPoolExecutor extends AbstractTimingThreadPoolExecutor implements NameExecutorService {
    protected static final String DEFAULT_TASK_NAME = "默认名称任务";

    /**
     * 任务总数
     */
    private final Map<String, AtomicLong> numTasksMap = new ConcurrentHashMap<>();
    /**
     * 总耗时
     */
    private final Map<String, AtomicLong> totalTimeMap = new ConcurrentHashMap<>();
    /**
     * 拒绝任务数
     */
    private final AtomicLong rejectNum = new AtomicLong();

    public NameTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                        TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        this(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
                new ThreadFactoryBuilder().setNameFormat(name + "-").build());
    }

    public NameTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime,
                                        TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        this(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, new CountCallerRunsPolicy());
    }

    public NameTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize,
                                        long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue,
                                        ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        this(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler, new LogNameExecuteInfoWriter());
    }

    public NameTimingThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize,
                                        long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue,
                                        ThreadFactory threadFactory, RejectedExecutionHandler handler,
                                        ExecuteInfoWriter executeInfoWriter) {
        super(name, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler, executeInfoWriter);
    }

    @Override
    public long incrementAndGetRejectNum(Runnable task) {
        return rejectNum.incrementAndGet();
    }


    @Override
    protected long incrementAndGetNumTasks(Runnable task) {
        String name = DEFAULT_TASK_NAME;
        if (task instanceof NameFutureTask) {
            NameFutureTask nameFutureTask = (NameFutureTask) task;
            name = nameFutureTask.getName();
        }

        if (numTasksMap.get(name) == null) {
            AtomicLong initValue = new AtomicLong(1);
            AtomicLong count = numTasksMap.putIfAbsent(name, initValue);
            if (count == null) {
                return initValue.get();
            }
        }
        AtomicLong count = numTasksMap.get(name);
        return count.incrementAndGet();

    }

    @Override
    protected long addAndGetTotalTime(long spendTime, Runnable task) {
        String name = DEFAULT_TASK_NAME;
        if (task instanceof NameFutureTask) {
            NameFutureTask nameFutureTask = (NameFutureTask) task;
            name = nameFutureTask.getName();
        }

        if (totalTimeMap.get(name) == null) {
            AtomicLong initValue = new AtomicLong(spendTime);
            AtomicLong count = totalTimeMap.putIfAbsent(name, initValue);
            if (count != null) {
                return initValue.get();
            }
        }
        AtomicLong count = totalTimeMap.get(name);
        return count.addAndGet(spendTime);
    }

    @Override
    public void dealExecuteInfo(ExecuteInfoHandler handler) {
        Map<String, AtomicLong> totalTimeMap = new ConcurrentHashMap<>(this.totalTimeMap);
        Map<String, AtomicLong> numTasksMap = new ConcurrentHashMap<>(this.numTasksMap);
        for (Map.Entry<String, AtomicLong> entry : totalTimeMap.entrySet()) {
            handler.dealInfo(getPoolName(), entry.getKey(), numTasksMap.get(entry.getKey()).get(), entry.getValue().get(), rejectNum.get());
        }
    }

    @Override
    public Object getLockObject(Runnable runnable) {
        //如果相应任务名的执行数为null,则使用线程池对象返回
        if (runnable instanceof NameFutureTask) {
            NameFutureTask nameFutureTask = (NameFutureTask) runnable;
            AtomicLong atomicLong = numTasksMap.get(nameFutureTask.getName());
            if (atomicLong != null) {
                return atomicLong;
            }
            System.out.println(nameFutureTask.getName() + ":返回线程池作为锁");
        }
        return this;
    }

    @Override
    public NameFutureTask submit(String name, Runnable runnable) {
        NameFutureTask futureTask = new NameFutureTask(name, runnable);
        execute(futureTask);
        return futureTask;
    }

    @Override
    public <T> NameFutureTask<T> submit(String name, Callable<T> callable) {
        NameFutureTask<T> futureTask = new NameFutureTask(name, callable);
        execute(futureTask);
        return futureTask;
    }

    @Override
    public NameFutureTask submit(Runnable runnable) {
        NameFutureTask futureTask = new NameFutureTask(DEFAULT_TASK_NAME, runnable);
        execute(futureTask);
        return futureTask;
    }

    @Override
    public <T> NameFutureTask<T> submit(Callable<T> callable) {
        NameFutureTask<T> futureTask = new NameFutureTask(DEFAULT_TASK_NAME, callable);
        execute(futureTask);
        return futureTask;
    }


    public static class LogNameExecuteInfoWriter implements ExecuteInfoWriter {
        private static final Logger logger = LoggerFactory.getLogger(LogNameExecuteInfoWriter.class);

        @Override
        public void writeExecuteInfo(String poolName, Runnable runnable, Long numTasks, Long totalSpendTime, Long currentSpendTime) {
            if (runnable instanceof NameFutureTask) {
                logger.info("线程池：{},任务名：{}，总任务数：{}，平均执行耗时：{}μs，本次执行耗时：{}μs", poolName, ((NameFutureTask) runnable).getName(), numTasks, (totalSpendTime / numTasks) / 1000, currentSpendTime / 1000);
            } else {
                logger.info("线程池：{},总任务数：{}，平均执行耗时：{}μs，本次执行耗时：{}μs", poolName, numTasks, (totalSpendTime / numTasks) / 1000, currentSpendTime / 1000);
            }

        }

        @Override
        public void writeReject(String poolName, Long rejectNum) {
            logger.warn("{} reject task num :{}", poolName, rejectNum);
        }

        @Override
        public void dealInfo(String poolName, String taskName, Long numTasks, Long totalSpendTime, Long rejectNum) {
            if (numTasks == 0) {
                return;
            }
            if (taskName != null) {
                logger.info("线程池：{}，任务名称：{}，总耗时：{} 平均耗时:{}μs,执行任务数：{},线程池总拒绝数：{}",
                        poolName, taskName, totalSpendTime / 1000, (totalSpendTime / numTasks) / 1000, numTasks, rejectNum);
            } else {
                logger.info("线程池：{}，总耗时：{}，平均耗时:{}μs,执行任务数：{}，线程池总拒绝数：{}", poolName, totalSpendTime / 1000, (totalSpendTime / numTasks) / 1000, numTasks, rejectNum);
            }
        }
    }
}
