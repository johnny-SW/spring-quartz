package com.example.demo.springsecurity.current.test;

import com.example.demo.springsecurity.current.AbstractTimingThreadPoolExecutor;
import com.example.demo.springsecurity.current.NameTimingThreadPoolExecutor;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: liucheng
 * @createTime:2020/7/3 11:09
 */
public class NameTimeThreadPoolExecutor {
    public static void main(String[] args) throws InterruptedException {
        NameTimingThreadPoolExecutor executor = new NameTimingThreadPoolExecutor("TestNameTimingThreadPoolExecutor", 30, 30, 0L,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100),
                new ThreadFactoryBuilder().setNameFormat("testTiming").build(),
                new AbstractTimingThreadPoolExecutor.CountCallerRunsPolicy());
//        final String[] name = new String[]{"测试1", "测试2", "测试3"};
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                executor.submit( () -> {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }).start();
        }

        TimeUnit.SECONDS.sleep(50);
        executor.shutdown();
    }
}
