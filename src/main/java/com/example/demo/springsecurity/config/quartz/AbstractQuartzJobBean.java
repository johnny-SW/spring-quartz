package com.example.demo.springsecurity.config.quartz;

import org.quartz.*;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.HashSet;
import java.util.Set;

/**
 * @description:任务类实现该类，实现相应的getJobName,getJobGroup,getCron方法就能被QUARTZ调度。
 * @author: liucheng
 * @createTime:2020/6/28 20:52
 */
public abstract class AbstractQuartzJobBean extends QuartzJobBean implements Schedulable {

    @Override
    public JobDetail getJobDetail() {
        return JobBuilder.newJob(this.getClass()).storeDurably(true).withIdentity(getJobName(), getJobGroup()).build();
    }

    @Override
    public Set<Trigger> getTriggers() {
        HashSet<Trigger> triggers = new HashSet<>(1);
        triggers.add(TriggerBuilder.newTrigger()
                .withIdentity(getJobName(), getJobGroup())
                .withSchedule(CronScheduleBuilder.cronSchedule(getCron())).build());
        return triggers;
    }

    /**
     * 任务名称
     *
     * @return
     */
    protected abstract String getJobName();

    /**
     * 任务组
     *
     * @return
     */
    protected abstract String getJobGroup();


    /**
     * 获取Cron表达式
     *
     * @return
     */
    protected abstract String getCron();
}
