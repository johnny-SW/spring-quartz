package com.example.demo.springsecurity.config.quartz;

import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description:启动注册任务，所有实现Scheduler的类都会被注入进来
 * @author: liucheng
 * @createTime:2020/6/28 21:08
 */
@Component
public class QuartzJobRegister implements ApplicationRunner {
    @Autowired
    private List<Schedulable> schedulables;

    @Autowired
    private Scheduler scheduler;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (Schedulable schedule : schedulables) {
            this.scheduler.scheduleJob(schedule.getJobDetail(), schedule.getTriggers(), true);
        }

    }

}
