package com.example.demo.springsecurity.config.quartz;

import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.util.Set;

/**
 * 实现该接口的类会被Quartz调度器调度
 *
 * @author lc
 */
public interface Schedulable {

    /**
     * 任务详情
     *
     * @return
     */
    JobDetail getJobDetail();

    /**
     * 获取触发时间
     *
     * @return
     * @author
     */
    Set<Trigger> getTriggers();
}
